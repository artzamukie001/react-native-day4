import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

export default class Page1 extends Component {
    reder() {
        const { todos } = this.props
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => { }}>
                    <Text>Add Number to Todo</Text>
                </TouchableOpacity>
                <Text>
                    Latest to is: {todos[todos.length - 1]}
                </Text>
            </View>
        )
    }
}