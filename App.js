import React, { Component } from 'react'
import { Provider } from 'react-redux'
import Page1 from './Page1'

import store from './AppStore';
export default class App extends Component {
  render(){
    return (
      <Provider store={store}>
        <Page1 />
      </Provider>
    )
  }
}